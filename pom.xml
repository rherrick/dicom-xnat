<?xml version="1.0" encoding="UTF-8"?>
<!--
  ~ dicom-xnat: pom.xml
  ~ XNAT https://www.xnat.org
  ~ Copyright (c) 2020, Washington University School of Medicine
  ~ All Rights Reserved
  ~
  ~ Released under the Simplified BSD.
  -->

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>org.nrg</groupId>
        <artifactId>parent</artifactId>
        <version>1.9.1</version>
    </parent>

    <artifactId>dicom-xnat</artifactId>
    <packaging>pom</packaging>

    <name>XNAT DICOM Support Libraries</name>
    <url>https://www.xnat.org</url>

    <properties>
        <xnat.dicom.version>${project.version}</xnat.dicom.version>
        <xnat.compiler.version>1.8</xnat.compiler.version>
    </properties>

    <modules>
        <module>dicom-xnat-sop</module>
        <module>dicom-xnat-util</module>
        <module>dicom-xnat-mx</module>
    </modules>

    <developers>
        <developer>
            <id>karchie</id>
            <name>Kevin A. Archie</name>
            <email>karchie@wustl.edu</email>
            <organization>Washington University Neuroinformatics Research Group</organization>
            <organizationUrl>https://nrg.wustl.edu</organizationUrl>
            <timezone>-6</timezone>
            <roles>
                <role>developer</role>
            </roles>
        </developer>
    </developers>

    <scm>
        <url>https://bitbucket.org/nrg/dicom-xnat</url>
    </scm>

    <organization>
        <name>Washington University Neuroinformatics Research Group</name>
        <url>https://nrg.wustl.edu</url>
    </organization>

    <build>
        <plugins>
            <plugin>
                <artifactId>maven-compiler-plugin</artifactId>
            </plugin>
            <plugin>
                <artifactId>maven-javadoc-plugin</artifactId>
                <configuration>
                    <quiet>true</quiet>
                    <additionalparam>-Xdoclint:none</additionalparam>
                </configuration>
            </plugin>
            <plugin>
                <artifactId>maven-source-plugin</artifactId>
            </plugin>
        </plugins>
    </build>

    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>org.nrg</groupId>
                <artifactId>dicom-xnat-mx</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>org.nrg</groupId>
                <artifactId>dicom-xnat-sop</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>org.nrg</groupId>
                <artifactId>dicom-xnat-util</artifactId>
                <version>${project.version}</version>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <repositories>
        <repository>
            <id>org.nrg.maven.artifacts.release</id>
            <name>XNAT Release Maven Repo</name>
            <url>https://nrgxnat.jfrog.io/nrgxnat/libs-release</url>
            <releases>
                <enabled>true</enabled>
                <updatePolicy>daily</updatePolicy>
                <checksumPolicy>fail</checksumPolicy>
            </releases>
            <snapshots>
                <enabled>false</enabled>
            </snapshots>
        </repository>
        <repository>
            <id>org.nrg.maven.artifacts.snapshot</id>
            <name>xnat snapshot maven repo</name>
            <url>https://nrgxnat.jfrog.io/nrgxnat/libs-snapshot</url>
            <releases>
                <enabled>false</enabled>
            </releases>
            <snapshots>
                <enabled>true</enabled>
                <updatePolicy>always</updatePolicy>
                <checksumPolicy>fail</checksumPolicy>
            </snapshots>
        </repository>
    </repositories>

</project>
